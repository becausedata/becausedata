# Because Data

A support group for the data curious

## Our One Job

Create a welcoming environment for sharing data.

## Code of Conduct

A code of conduct applies wherever we gather to talk data. This is true both in person and online.

* We want to be inclusive. Don’t engage in homophobic, racist, transphobic, ableist, sexist, or otherwise exclusionary behavior. Don’t make exclusionary jokes. Don’t even make them “ironically.”
* Don’t harass people. Unconsented physical contact or sexual attention is harrassment. Dressing or acting in a certain way is not consent.
* Aggression and elitism are unwelcome here. Data-ing is not a competition.
* Although some groups may meet in a pub, there is no expectation or pressure to drink alcohol. Don’t question anyone’s choice of drink.
* We’d rather you ask about gender than assume, and if you get it wrong, apologise, and use what they prefer.
* We think feminism is a good thing. Discussion of how to make data more inclusive is welcome. Claims that this “has gone too far” aren’t.
* It might be a good idea to read about [being a good ally](http://theangryblackwoman.com/2009/10/01/the-dos-and-donts-of-being-a-good-ally/) or some [specific resources for various issues](http://superopinionated.com/101-a-tron/) if you’re worried about offending people.

Copied with small changes from the [Computer Anonymous](http://www.computeranonymous.org) public domain code of conduct.

## Contact

[@becausedata](https://twitter.com/becausedata)

## License

[CC0](http://creativecommons.org/publicdomain/zero/1.0/)
To the extent possible under law, the person who associated CC0 with this work has waived all copyright and related or neighboring rights to this work.
